﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircularBumper: MonoBehaviour {

    [SerializeField]
    [Range(1f,4.0f)]
    private float bumperForce =1f;

    

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Ball")
        {

            foreach (ContactPoint contact in collision.contacts)
            {
                GameObject.Find("GAME MANAGER").GetComponent<GameManager>().AddScore(300);
                contact.otherCollider.GetComponent<Rigidbody>().AddForce(-1 * contact.normal * bumperForce, ForceMode.Impulse);
                GetComponent<AudioSource>().Play();

            }
        }
    }

}
