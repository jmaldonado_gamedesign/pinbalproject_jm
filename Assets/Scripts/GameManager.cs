﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {


    public int score = 0;
    public int ballsLeft = 0;
    public int HighScore = 0;

    public static int multiplier = 1;

    public enum GameState { Start, End, Playing}

    public static GameState gState;

    bool startButton = false;

    private string TextScore;

    private string MultiTxt;

    [SerializeField]
    GameObject ScoreforText;

    [SerializeField]
    GameObject TextforHighScore;

    [SerializeField]
    GameObject MultiplierObject;

    [SerializeField]
    GameObject TextBall;

    [SerializeField]
    GameObject PrefabBall;

    [SerializeField]
    GameObject StartText;

    [SerializeField]
    GameObject EndText;



    // Use this for initialization
    void Start () {

        score = 0;
        ballsLeft = 0;
        HighScore = 20000;
        gState = GameState.Start;
        //START TEXT
        StartText.SetActive(true);
        EndText.SetActive(false);

        multiplier = 1;

        GameObject.Find("Main Camera").GetComponent<Camera>().enabled = true;
        GameObject.Find("UpperFlipperCamera").GetComponent<Camera>().enabled = false;
        

    }
	
	// Update is called once per frame
	void Update () {

        if (gState == GameState.Start)
        {
            startButton = Input.GetKeyDown(KeyCode.Return);

            
            

            if (startButton)
            {
                //Deactivate the StartHud
                StartText.SetActive(false);
                //PutVars
                score = 0;
                ballsLeft = 3;
                gState = GameState.Playing;
                UpdateBallCount();

                //Instance the ball
                Instantiate(PrefabBall, transform.GetChild(0).transform.position, Quaternion.Euler(Vector3.zero), null);
                
            }
        }
        else if(gState == GameState.Playing)
        {

            UpdateScores();

            if (ballsLeft <= 0)
            {
                gState = GameState.End;
                EndGame();
            }
        }
        else if(gState == GameState.End)
        {

            GetComponent<AudioSource>().volume -= 0.005f;

            if(GetComponent<AudioSource>().volume <= 0)
            {
                if (GetComponent<AudioSource>().isPlaying)
                    GetComponent<AudioSource>().Stop();
            }

            startButton = Input.GetKeyDown(KeyCode.Return);
            if (startButton)
            {
                Application.LoadLevel(Application.loadedLevel);

            }

        }

	}

    void EndGame()
    {
        EndText.SetActive(true);

        EndText.transform.GetChild(0).GetComponent<TextMesh>().text += score.ToString();
        if(score > HighScore)
        {
            EndText.transform.GetChild(1).gameObject.SetActive(true);
        }

    }

    void UpdateScores()
    {
        int tempnum = 0;
        int tempscore = score;

        TextScore = "";

        while(tempscore > 10)
        {
            tempscore /= 10;
            tempnum++;
        }

        tempnum = 9 - tempnum;
        for(int i = 0; i < tempnum; i++)
        {
            TextScore += "0";
        }
        TextScore += score.ToString();

        ScoreforText.GetComponent<TextMesh>().text = TextScore;


        //UPDATE THE HIGHSCORE
        if(score < HighScore)
        {

            TextScore = "";
            tempscore = HighScore;
            tempnum = 0;
            while (tempscore > 10)
            {
                tempscore /= 10;
                tempnum++;
            }
            tempnum = 9 - tempnum;
            for (int i = 0; i < tempnum; i++)
            {
                TextScore += "0";
            }
            TextScore += HighScore.ToString();
        }

        TextforHighScore.GetComponent<TextMesh>().text = TextScore;

        //UPDATE THE MULTIPLIER

        MultiTxt = "X" + multiplier.ToString();

        MultiplierObject.GetComponent<TextMesh>().text = MultiTxt;
    }


    public void UpdateBallCount()
    {
        TextBall.GetComponent<TextMesh>().text = ballsLeft.ToString();
        


    } 

    public void AddScore(int points)
    {
        score += points * multiplier;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Ball")
        {

            ballsLeft--;
            UpdateBallCount();
            transform.GetChild(1).GetComponent<AudioSource>().Play();

        }
    }

}
