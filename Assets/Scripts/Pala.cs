﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pala : MonoBehaviour
{

    private enum PaddleType{Left, Right};

    [SerializeField]
    PaddleType TypePaddle;


    JointSpring spring;
    HingeJoint hingeJoint;
    [SerializeField] float restPosition = 0F;
    [SerializeField] float pressedPosition = 45F;
    [SerializeField] float flipperStrength = 1000F;
    [SerializeField] float flipperDamper = 100F;
    [SerializeField] float direction;



    bool turn;

    // Use this for initialization
    void Start()
    {
        hingeJoint = GetComponent<HingeJoint>();


        spring = new JointSpring();
        spring.spring = flipperStrength;
        spring.damper = flipperDamper;
        spring.targetPosition = restPosition;
        hingeJoint.spring = spring;
        hingeJoint.useSpring = true;

    }

    void FixedUpdate()
    {

        if (TypePaddle == PaddleType.Right)
            turn = Input.GetKey("right");
        else if(TypePaddle == PaddleType.Left)
            turn = Input.GetKey("left");

        if (GameManager.gState == GameManager.GameState.Playing)
        {

            if (turn)
            {
                spring.targetPosition = pressedPosition;
                hingeJoint.spring = spring;
            }
            else
            {
                spring.targetPosition = restPosition;
                hingeJoint.spring = spring;
            }

        }
    }
}