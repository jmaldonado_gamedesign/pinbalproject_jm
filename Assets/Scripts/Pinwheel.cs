﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pinwheel : MonoBehaviour {


    public int DirectionChanger = -1;

    float timepassed = 0;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        timepassed += Time.deltaTime;

        if(timepassed > 20){

            DirectionChanger *= -1;
            timepassed = 0;
        }


        gameObject.transform.Rotate(new Vector3(0, 0, 20 * DirectionChanger), Time.deltaTime * 20);

    }
}
