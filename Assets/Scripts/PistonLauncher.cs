﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PistonLauncher : MonoBehaviour
{

    [SerializeField]
    [Range(1f, 50f)]
    private float bumperForce = 50f;

    private bool downPressed;

    Rigidbody rb;



    public void Start()
    {
        rb = gameObject.GetComponent<Rigidbody>();

    }


    public void Update()
    {
        

        downPressed = Input.GetKey(KeyCode.Space);

        if (rb.IsSleeping())
        {
            rb.WakeUp();
        }



    }

    private void OnCollisionStay(Collision collision)
    {

        if (GameManager.gState == GameManager.GameState.Playing)
        {

            if (downPressed && collision.gameObject.tag == "Ball")
            {

                foreach (ContactPoint contact in collision.contacts)
                {

                    contact.otherCollider.GetComponent<Rigidbody>().AddForce(Vector3.forward * bumperForce, ForceMode.Impulse);
                    transform.GetChild(0).GetComponent<ParticleSystem>().Play();
                    GetComponent<AudioSource>().Play();

                }
            }

        }
    }



}
