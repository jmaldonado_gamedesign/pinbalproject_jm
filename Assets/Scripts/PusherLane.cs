﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PusherLane : MonoBehaviour {

    enum Forces { up, back, foward, left, right}

    [SerializeField]
    Forces TypeForce = Forces.back;

    private Vector3 force;

    [SerializeField]
    [Range(1f, 5000f)]
    float Impulse = 1f;


    private void Start()
    {

        switch (TypeForce)
        {
            case Forces.back: default:
                force = Vector3.back;
                break;
            case Forces.up:
                force = Vector3.up;
                break;
            case Forces.foward:
                force = Vector3.forward;
                break;
            case Forces.left:
                force = Vector3.left;
                break;
            case Forces.right:
                force = Vector3.right;
                break;
        }

    }


    private void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.tag == "Ball")
        {
            GameObject.Find("GAME MANAGER").GetComponent<GameManager>().AddScore(1500);
            collision.GetComponent<Rigidbody>().AddForce(force * Impulse, ForceMode.Force);
            GetComponent<AudioSource>().Play();
        }
    }
}
