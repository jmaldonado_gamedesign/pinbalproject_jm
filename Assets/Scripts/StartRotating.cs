﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartRotating : MonoBehaviour {


    private bool Starting = false;

    private float timeUntilCameraChange = 0;
    private bool CameraNeedsChange = false;

    private int DirectionChanger = 1;
    float timepassed = 0;

    public GameObject Parent;

    Camera MainCamera;
    Camera CannonCamera;

    private void Start()
    {

        MainCamera = GameObject.Find("Main Camera").GetComponent<Camera>();
        CannonCamera = GameObject.Find("UpperFlipperCamera").GetComponent<Camera>();

    }


    private void Update()
    {
        if (Starting)
        {

            timepassed += Time.deltaTime;

            if (timepassed > 4)
            {

                DirectionChanger *= -1;
                timepassed = 0;
            }
            Parent.transform.Rotate(new Vector3(20 * DirectionChanger, 0, 0), Time.deltaTime * 20);

            
        }
        else
        {

            if (CameraNeedsChange)
            {

                timeUntilCameraChange += Time.deltaTime;

                if (timeUntilCameraChange > 1.5)
                {

                    //We change the display
                    MainCamera.enabled = true;
                    CannonCamera.enabled = false;
                    CameraNeedsChange = false;
                    timeUntilCameraChange = 0;

                }

            }

        }
    }

    private void OnTriggerEnter(Collider other)
    {
        
        if(other.gameObject.tag == "Ball")
        {
            
            Starting = true;
            //We change the display
            MainCamera.enabled = false;
            CannonCamera.enabled = true;
        }            

    }

    private void OnTriggerExit(Collider other)
    {

        if (other.gameObject.tag == "Ball")
        {

            Starting = false;
            CameraNeedsChange = true;

        }

    }
}
