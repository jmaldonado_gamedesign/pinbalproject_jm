﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Target : MonoBehaviour {


    [SerializeField]
    public bool IsInASet = false;

    public Animator animator;

    bool hit=false;

    float timer = 0;

    [SerializeField]
    float MaxTime = 10f;

	// Use this for initialization
	void Start () {

        animator = GetComponent<Animator>();
        gameObject.transform.GetChild(0).gameObject.SetActive(false);

    }
	
	// Update is called once per frame
	void Update () {

        if (animator.GetBool("Hit"))
        {

            if (!IsInASet)
            { 
                timer += Time.deltaTime;
                if(timer > MaxTime)
                {
                    timer = 0;
                    animator.SetBool("Hit", false);
                    gameObject.transform.GetChild(0).gameObject.SetActive(false);
                }
            }
        }

    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Ball")
        {

            animator.SetBool("Hit", true);
            gameObject.transform.GetChild(0).gameObject.SetActive(true);
            GameObject.Find("GAME MANAGER").GetComponent<GameManager>().AddScore(100);
            GetComponent<AudioSource>().Play();


            if (IsInASet)
                gameObject.transform.GetComponentInParent<TargetSet>().TargetsDone++;

        }
    }

}
