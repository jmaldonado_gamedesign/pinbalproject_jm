﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetSet : MonoBehaviour {

    [SerializeField]
    GameObject[] ArraySets;

    public int TargetsInASet = 0;
    public int TargetsDone = 0;


    float timer = 0;

    private void Start()
    {
        for (int i = 0; i < gameObject.transform.childCount; i++)
        {

            if (gameObject.transform.GetChild(i).GetChild(0).gameObject.GetComponent<Target>().IsInASet)
            {
                TargetsInASet++;
            }
        }
        ArraySets = new GameObject[TargetsInASet];

        for (int i = 0; i < gameObject.transform.childCount; i++)
        {

            if (gameObject.transform.GetChild(i).GetChild(0).gameObject.GetComponent<Target>().IsInASet)
            {
                ArraySets[i] = gameObject.transform.GetChild(i).GetChild(0).gameObject;
                
            }
        }

        

    }

    private void Update()
    {

        if (TargetsDone >= TargetsInASet)
        {

            timer += Time.deltaTime;

            if (timer > 2)
            {
                TargetsDone = 0;
                timer = 0;
                GameManager.multiplier += 1;
                if(GameManager.multiplier >= 10)
                {
                    GameManager.multiplier = 10;
                }
                foreach (GameObject t in ArraySets)
                {
                    t.GetComponent<Target>().animator.SetBool("Hit", false);
                    t.transform.GetChild(0).gameObject.SetActive(false);
                }
                
            }
        }

    }



}
