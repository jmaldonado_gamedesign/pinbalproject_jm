﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriangularBumper : MonoBehaviour {

    [SerializeField]
    [Range(1f,20f)]
    private float bumperForce =1f;

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Ball")
        {

            foreach (ContactPoint contact in collision.contacts)
            {
                GameObject.Find("GAME MANAGER").GetComponent<GameManager>().AddScore(400);
                contact.otherCollider.GetComponent<Rigidbody>().AddForce(-1 * contact.normal * bumperForce, ForceMode.Impulse);
                GetComponent<AudioSource>().Play();

            }
        }
    }

}
